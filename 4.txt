
https://raw.githubusercontent.com/rancher/k3d/main/install.sh

wget -q -O - https://raw.githubusercontent.com/rancher/k3d/main/install.sh | bash



function animateNumber(finalNumber, delay, startNumber = 0, callback) {
  let currentNumber = startNumber
  const interval = window.setInterval(updateNumber, delay)
  function updateNumber() {
    if (currentNumber >= finalNumber) {
      clearInterval(interval)
    } else {
      currentNumber++
    }
    callback(currentNumber)
  }
}




<!DOCTYPE html>
<html>
<body>

<h1>The Window Object</h1>
<h2>The setInterval() Method</h2>

<p id="demo"></p>

<script>
const qq = setInterval(displayHello, 1000);


function displayHello() {
   document.getElementById("demo").innerHTML += "---" + qq; 
}
</script>

</body>
</html>


const observer = new IntersectionObserver(callback)

let callback = (entries, observer) => {
  entries.forEach(entry => {
    entry.intersectionRatio // từ 0 đến 1
    entry.target // HTMLElement
  })
}

const target = document.querySelector('#elem')
observer.observe(target)

=============================================



const observer = new IntersectionObserver((entries, observer) => {
  entries.forEach(entry => {
    if (entry.intersectionRatio === 1) {
      entry.target.classList.add('bg-blue-600')
      entry.target.classList.remove('bg-gray-200')
    } else {
      entry.target.classList.remove('bg-blue-600')
      entry.target.classList.add('bg-gray-200')
    }
  })
}, {
  threshold: 1
})

const elements = document.querySelectorAll('.item')

elements.forEach(element => {
  observer.observe(element)
})
